#!/usr/bin/env bash

# Colors
Black="\033[0;30m"
Red="\033[0;31m"
Green="\033[0;32m"
Yellow="\033[0;33m"
Blue="\033[0;34m"
Magenta="\033[0;35m"
Cyan="\033[0;36m"
White="\033[0;37m"
NC="\033[0m" # No Color

# User agent
USER_AGENT="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0"

echo -e "${Blue}Media Check${NC}"
echo -e "${Blue}https://gitlab.com/hunter/media-check${NC}"

[[ ! $(command -v curl) ]] && [[ $(whoami) == "root" ]] && [[ $(command -v apt) ]] && apt install -y curl >> /dev/null 2>&1
[[ ! $(command -v curl) ]] && [[ $(whoami) == "root" ]] && [[ $(command -v dnf) ]] && dnf install -y curl >> /dev/null 2>&1
[[ ! $(command -v curl) ]] && [[ $(whoami) == "root" ]] && [[ $(command -v yum) ]] && yum install -y curl >> /dev/null 2>&1

function netflix_checker_ipv4() {
	echo
	echo "Use IPv4 for testing"

	result=$(curl -4sSLA "$USER_AGENT" "https://www.netflix.com/" 2>&1)
	if [[ $result == "curl:"* ]]; then
		echo -e "${Red}Unable to connect Netflix${NC}"
		return
	fi
	if [[ $result == "Not Available" ]]; then
		echo -e "${Red}Unable to use Netflix${NC}"
		return
	fi

	# Area
	mkdir -p /dev/shm/.media-check
	echo $result > /dev/shm/.media-check/index.html
	if [[ $result == *"\"footer-country\""* ]]; then
		sed -i "/\"footer-country\"/!d" /dev/shm/.media-check/index.html
		sed -i "s/.*\"footer-country\">Netflix[[:blank:]]//" /dev/shm/.media-check/index.html
		sed -i "s/<.*//" /dev/shm/.media-check/index.html
		area=$(cat /dev/shm/.media-check/index.html)
	elif [[ $result == *"United States"* ]]; then
		area="United States"
	else
		area="Global"
	fi
	echo -e "${Cyan}${area}${NC}"
	rm -rf /dev/shm/.media-check

	# Breaking Bad
	check_available=$(curl -4sLA "$USER_AGENT" "https://www.netflix.com/title/70143836" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# Prison Break
	check_available=$(curl -4sLA "$USER_AGENT" "https://www.netflix.com/title/70140425" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# Sherlock
	check_available=$(curl -4sLA "$USER_AGENT" "https://www.netflix.com/title/70202589" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# The Flash
	check_available=$(curl -4sLA "$USER_AGENT" "https://www.netflix.com/title/80027042" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# The Originals
	check_available=$(curl -4sLA "$USER_AGENT" "https://www.netflix.com/title/70283261" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# The Vampire Diaries
	check_available=$(curl -4sLA "$USER_AGENT" "https://www.netflix.com/title/70143860" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return

	echo -e "${Yellow}Netflix original programming is available${NC}"
}

function netflix_checker_ipv6() {
	echo
	echo "Use IPv6 for testing"

	result=$(curl -6sSLA "$USER_AGENT" "https://www.netflix.com/" 2>&1)
	if [[ $result == "curl:"* ]]; then
		echo -e "${Red}Unable to connect Netflix${NC}"
		return
	fi
	if [[ $result == "Not Available" ]]; then
		echo -e "${Red}Unable to use Netflix${NC}"
		return
	fi

	# Area
	mkdir -p /dev/shm/.media-check
	echo $result > /dev/shm/.media-check/index.html
	if [[ $result == *"footer-country"* ]]; then
		sed -i "/\"footer-country\"/!d" /dev/shm/.media-check/index.html
		sed -i "s/.*\"footer-country\">Netflix[[:blank:]]//" /dev/shm/.media-check/index.html
		sed -i "s/<.*//" /dev/shm/.media-check/index.html
		area=$(cat /dev/shm/.media-check/index.html)
	elif [[ $result == *"United States"* ]]; then
		area="United States"
	else
		area="Global"
	fi
	echo -e "${Cyan}${area}${NC}"
	rm -rf /dev/shm/.media-check

	# Breaking Bad
	check_available=$(curl -6sLA "$USER_AGENT" "https://www.netflix.com/title/70143836" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# Prison Break
	check_available=$(curl -6sLA "$USER_AGENT" "https://www.netflix.com/title/70140425" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# Sherlock
	check_available=$(curl -6sLA "$USER_AGENT" "https://www.netflix.com/title/70202589" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# The Flash
	check_available=$(curl -6sLA "$USER_AGENT" "https://www.netflix.com/title/80027042" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# The Originals
	check_available=$(curl -6sLA "$USER_AGENT" "https://www.netflix.com/title/70283261" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return
	# The Vampire Diaries
	check_available=$(curl -6sLA "$USER_AGENT" "https://www.netflix.com/title/70143860" 2>&1)
	[[ $check_available ]] && [[ $check_available != "" ]] && [[ $check_available != *"NSES-404"* ]] && [[ $check_available != *"page-404"* ]] && echo -e "${Green}All shows on Netflix is available${NC}" && return

	echo -e "${Yellow}Netflix original programming is available${NC}"
}

if [[ $0 == "bash" ]] || [[ $1 == "-f" ]] || [[ $1 == "--force" ]]; then
	netflix_checker_ipv4
	netflix_checker_ipv6
else
	ping -c 1 1.1.1.1 >> /dev/null 2>&1 && netflix_checker_ipv4
	ping -c 1 2606:4700:4700::1111 >> /dev/null 2>&1 && netflix_checker_ipv6
fi
